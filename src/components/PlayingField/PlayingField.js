import React from 'react';
import './PlayingField.css';

const PlayingField = props => {
    return (
            <div className="playingfield">
                <div className="field">
                    {props.children}
                </div>
            </div>
        )
};

export default PlayingField;
