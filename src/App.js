import React, { Component } from 'react';
import PlayingField from './components/PlayingField/PlayingField';
import Button from './components/Button/Button';
import './App.css';
import NumberOfAttempts from "./components/NumberOfAttempts/NumberOfAttempts";
// import OneCell from './components/OneCell/OneCell';

class App extends Component {
        state = {
            open: 'block'
        };

  classDisabled = () => {
 this.setState({
   open: 'blockNone'
 })
  };

  render() {
    const playingField = [];
    for (let i = 0; i < 36; i++) {
      playingField.push(i);
    }
    const number = Math.floor(Math.random() * playingField.length);
    delete playingField[number];
    playingField.splice(number, 1, 'x');

    const TableForGame = playingField.map((item) =>
            <div key={item} className={this.state.open} onClick={this.classDisabled.bind(this)}>
                <div key={item + 'x'} className="open">
                    {playingField[item]}
                </div>
            </div>
    );

    return (
      <div className="App">
        <p className="play">Let's play this game?</p>
        <PlayingField>
            {TableForGame}
        </PlayingField>
        <NumberOfAttempts/>
        <Button/>
      </div>
    );
  }
}

export default App;
